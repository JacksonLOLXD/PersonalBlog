<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::post('logout',['as' => 'logout', 'uses' => "Auth\LoginController@logout"]);
Route::get('logout', function(){
	Auth::logout();
	return redirect('/');
});

//Rutas para crear un perfil de usuario
Route::resource('user','PeopleController');

Route::put('user/update', ['as' => 'user/update', 'uses' => 'PeopleController@update']);


//Rutas para crear un perfil de usuario
Route::resource('post','PostController');

Route::put('post/update', ['as' => 'post/update', 'uses' => 'PostController@update']);

Route::get('post/destroy/{id}', ['as' => 'post/destroy', 'uses' => 'PostController@destroy']);

Route::post('post/search', ['as' => 'post/search', 'uses' => 'PostController@search']);

//Ruta enviar mails
Route::post('/sendmail', function(\Illuminate\Http\Request $request, \Illuminate\Mail\Mailer $mailer){
    $mailer
        ->to($request->input('mail'))
        ->send(new \App\Mail\Welcome($request->input('title'),$request->input('content')));
    return redirect()->back();
})->name('sendmail');