<?php

use Illuminate\Database\Seeder;
use App\Models\Gender;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $genders = array(
        	[
        		'name_gender' => 'Masculino',
        	],
        	[
        		'name_gender' => 'Femenino',
        	],
        	[
        		'name_gender' => 'Indefinido',
        	],
        );

        foreach ($genders as $value){
        	$gender = new Gender;
        	$gender->name_gender = $value['name_gender'];
        	$gender->save();
        }
    }
}
