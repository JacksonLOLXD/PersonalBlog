<?php

use Illuminate\Database\Seeder;
use App\Models\People;

class PeopleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $peoples = array(
            [
                'first_name' => 'Jair',
                'last_name' => 'jack@gmail.com',
                'birth_date' => '2000-03-20',
                'gender_id' => 1,
            ],
        );

        foreach ($peoples as $value){
            $people = new People;
            $people->first_name = $value['first_name'];
            $people->last_name = $value['last_name'];
            $people->birth_date = $value['birth_date'];
            $people->gender_id = $value['gender_id'];
            $people->user_id = $value['user_id'];
            $people->save();
        }
    }
}
