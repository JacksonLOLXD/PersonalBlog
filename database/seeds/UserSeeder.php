<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users = array(
            [
                'nick_name' => 'User',
                'email' => 'default@default.com',
                'password' => 'default',
                'type_user_id' => 1,
                'first_name' => 'default',
                'last_name' => 'default',
                'birth_date' => '2000-03-20',
            ],
            [
                'nick_name' => 'JacksonLOLXD',
                'email' => 'jack@gmail.com',
                'password' => '123456',
                'type_user_id' => 1,
                'first_name' => 'Jair',
                'last_name' => 'Cubillos',
                'birth_date' => '2000-03-20',
            ],
        );

        foreach ($users as $value){
            $user = new User;
            $user->nick_name = $value['nick_name'];
            $user->email = $value['email'];
            $user->password = bcrypt($value['password']);
            $user->type_user_id = $value['type_user_id'];
            $user->first_name = $value['first_name'];
            $user->last_name = $value['last_name'];
            $user->birth_date = $value['birth_date'];
            $user->save();
        }
    }
}
