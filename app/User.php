<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use App\Models\TypeUser;
use App\Models\Post;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $fillable = ['nick_name','email', 'password','type_user_id','first_name','profile_picture','last_name','birth_date'];
    protected $guarded = ['id'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function typeUser(){
    	return $this->belongsTo(TypeUser::class);
    }

    public function posts(){
        return $this->hasMany(Post::class);
    }
}
