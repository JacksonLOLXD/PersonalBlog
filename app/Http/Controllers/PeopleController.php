<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use Storage;

use App\User as User;

class PeopleController extends Controller
{
    private $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    public function index()
    {
        $users = User::all();
        return \View::make('peoples/listPerson', compact('users'));
    }

    public function create()
    {
        //
        return \View::make('peoples/newPerson');
    }

    public function store(Request $request)
    {
        //
        $img = $data->file('urlImg');
        $file_route = time().'_'.$img->getClientOriginalName();
        Storage::disk('imgProfiles')->put($file_route, file_get_contents($img->getRealPath()));

        $user = new User;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->nick_name = $request->nick_na;
        $user->profile_picture = $file_route;
        $user->birth_date = $request->birth_date;
        $user->save();
        //return \View::make('home');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
        $user = User::find($this->auth->user()->id);
        return \View::make('peoples/newPerson', compact('user'));
    }

    public function update(Request $request, $id)
    {
        //
        $user = User::find($this->auth->user()->id);
        $img = $request->file('urlImg');
        $file_route = time().'_'.$img->getClientOriginalName();
        Storage::disk('imgProfiles')->put($file_route, file_get_contents($img->getRealPath()));
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->nick_name = $request->nick_name;
        $user->profile_picture = $file_route;
        $user->birth_date = $request->birth_date;
        $user->save();
        return redirect('user');
    }

    public function destroy($id)
    {
        //
        $user = User::find($id);
        $user->delete();
        return \View::make('peoples/listPerson');
    }
}
