<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;

use App\Models\Post as Post;

class PostController extends Controller
{
    private $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    public function index()
    {
        $posts = Post::select('name_post','descripcion','content','user_id')
        ->where('user_id','=', $this->auth->user()->id)
        ->get();
        
        return \View::make('posts/listPost', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return \View::make('posts/newPost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $post = new Post;
        $post->name_post = $request->new_post;
        $post->descripcion = $request->descripcion;
        $post->content = $request->content;
        $post->user_id = $this->auth->user()->id;
        $post->save();
        return redirect('post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($this->auth->user()->id);
        return \View::make('post/updatePost', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $post = Post::find($id);
        $post->name_post = $request->new_post;
        $post->descripcion = $request->descripcion;
        $post->content = $request->content;
        $post->user_id = $this->auth->user()->id;
        $post->save();
        return redirect('post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);
        $post->delete();
        return \View::make('posts/listPost');
    }
}
