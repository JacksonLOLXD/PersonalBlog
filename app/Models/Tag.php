<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $table = 'tags';
    protected $fillable = ['name_tag'];
    protected $guarded = ['id'];

    public function detailTagPost(){
        return $this->hasMany(DetailTagPost::class);
    }
}
