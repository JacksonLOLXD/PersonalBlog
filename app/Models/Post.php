<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $table = 'posts';
    protected $fillable = ['name_post','descripcion','content','user_id'];
    protected $guarded = ['id'];

    public function usuario(){
        return $this->belongsTo(User::class);
    }

    public function detailTagPost(){
        return $this->hasMany(DetailTagPost::class);
    }
}
