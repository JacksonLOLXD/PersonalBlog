<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailTagPost extends Model
{
    //
    protected $table = 'detail_tag_posts';
    protected $fillable = ['post_id','tag_id'];
    protected $guarded = ['id'];

    public function post(){
        return $this->belongsTo(Post::class);
    }

    public function tag(){
        return $this->belongsTo(Tag::class);
    }
}
