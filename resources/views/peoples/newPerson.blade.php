@extends('layouts.app')
@section('content')
<div id="sss">
    @include('layouts.banner')
</div>
@include('layouts.menu')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-ms-offst-1">
			{!! Form::open(['route' => 'user/update', 'method' => 'put','enctype' => 'multipart/form-data']) !!}
			<div class="form-group">
				<label for="nick_name">Nombre de Usuario</label>
				<input type="text" name="nick_name" value="{{$user->nick_name}}" class="form-control" >
			</div>

			<div class="form-group">
				<label for="email">Correo</label>
				<input type="text" name="email" value="{{$user->email}}" class="form-control" >
			</div>

			<div class="form-group">
				<label for="first_name">Nombres</label>
				<input type="text" name="first_name" value="{{$user->first_name}}" class="form-control" >
			</div>
			<div class="form-group">
				<label for="last_name">Apellidos</label>
				<input type="text" name="last_name" value="{{$user->last_name}}" class="form-control" >
			</div>

            <div class="form-group{{ $errors->has('urlImg') ? ' has-error' : '' }}">
	            <label for="urlImg" class="col-md-4 control-label">Imagen de Perfil</label>
	            <div class="col-md-6">
	                <input id="urlImg" type="file" class="form-control" name="urlImg" value="{{$user->profle_picture}}" >
	            </div>
            </div>

			<div class="form-group">
				{!! Form::submit('Guardar tu perfil', ['class' => 'btn btn-submita']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection