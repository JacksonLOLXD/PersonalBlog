@extends('layouts.app')
    @section("content")
    <div id="sss">
        @include('layouts.banner')
    </div>
    @include('layouts.menu')
    <div class="container">
        <h1>Perfil</h1>
        <div class="col-md-3">
            <b>Imagen de Perfil: </b><br><img class="img-thumbnail" src="imgProfiles/{{\Auth::user()->profile_picture}}"><br>
            <center>
                <a class="btn btn-submita" href="{{ route('user.edit',['id' => \Auth::user()->id] )}}">Actualizar Perfil</a> 
            </center>
        </div>
        <div class="col-md-8">
            <p><b>Nombre de usuario: </b>{{ \Auth::user()->nick_name }}</p>
            <p><b>Correo: </b>{{ \Auth::user()->email }}</p>
            <p><b>Nombres: </b>{{ \Auth::user()->first_name }}</p>
            <p><b>Apellidos: </b>{{ \Auth::user()->last_name }}</p>
            <p><b>Fecha de Nacimiento: </b>{{ \Auth::user()->birth_date }}</p>    
        </div>
        
        @if(\Auth::user()->type_user_id == 3)
            <b>Usuarios Registrados</b>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Imagen de Perfil</th>
                            <th>Nombre de Usuario</th>
                            <th>Correo</th>
                            <th>Nombres y Apellidos</th>
                            <th>Fecha de Nacimiento</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td><img class="img-thumbnail" src="imgProfiles/{{\Auth::user()->profile_picture}}"></td>
                            <td>{{ $user->nick_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                            <td>{{ $user->birth_date }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    @endsection