@extends('layouts.app')
    @section("content")
    <div id="sss">
        @include('layouts.banner')
    </div>
    @include('layouts.menu')
    <div class="container">
        @foreach($posts as $post)
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nombre del Post</th>
                            <th>Descripcion</th>
                            <th>Contenido</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input type="text" name="name_post" value="{{ $post->name_post }}"></td>
                            <td><input type="textarea" name="descripcion" value="{{ $post->descripcion }}"></td>
                            <td><input type="textarea" name="content" value="{{ $post->content }}"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        @endforeach
        <a href="">Crear Post</a>
    </div>
    @endsection