@extends('layouts.app')
@section('content')
<div id="sss">
    @include('layouts.banner')
</div>
@include('layouts.menu')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-ms-offst-1">
			{!! Form::open(['route' => 'user.store', 'method' => 'post']) !!}
			<div class="form-group">
				<label for="name_post">Nombre del post</label>
				<input type="text" name="name_post"  class="form-control" >
			</div>

			<div class="form-group">
				<label for="descripcion">Descripcion</label>
				<input type="textarea" name="descripcion" class="form-control" >
			</div>
			<div class="form-group">
				<label for="content">Contenido</label>
				<input type="text" name="content" class="form-control" >
			</div>

			<div class="form-group">
				{!! Form::submit('Guardar tu perfil', ['class' => 'btn btn-submita']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection