@extends('layouts.app')
@section('content')
<div id="sss">
    @include('layouts.banner')
</div>
@include('layouts.menu')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-ms-offst-1">
			{!! Form::open(['route' => 'post/update', 'method' => 'put','enctype' => 'multipart/form-data']) !!}
			<div class="form-group">
				<label for="name_post">Nombre del post</label>
				<input type="text" name="name_post" value="{{$post->name_post}}" class="form-control" >
			</div>

			<div class="form-group">
				<label for="descripcion">Descripcion</label>
				<input type="textarea" name="descripcion" value="{{$post->descripcion}}" class="form-control" >
			</div>
			<div class="form-group">
				<label for="content">Contenido</label>
				<input type="text" name="content" value="{{$post->content}}" class="form-control" >
			</div>

			<div class="form-group">
				{!! Form::submit('Guardar tu perfil', ['class' => 'btn btn-submita']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection