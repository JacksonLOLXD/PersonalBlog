@extends('layouts.app')
<div id="app">
  <div class="navbar navbar-defaulta navbar-static-top">
    <div class="container">
      <div class="navbar-header">

        <!-- Collapsed Hamburger -->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
          <span class="sr-only">Toggle Navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <!-- Branding Image -->
        <a class="navbar-brande" href="{{ url('/') }}">
          {{ config('app.name', 'Blog') }}
        </a>
      </div>

      <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
        <ul class="nav navbar-nav">
          &nbsp;
        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
          <!-- Authentication Links -->
          @if (Auth::guest())
            <li><a href="{{ url('/login') }}">Login</a></li>
            <li><a href="{{ url('/register') }}">Register</a></li>
          @else
            <div class="dropdown">
              <button type="button" class="btn btn-submita dropdown-toggle" data-toggle="dropdown">
                Bienvenido {{ Auth::user()->nick_name }}
              </button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ url('/logout') }}">Salir</a></li>
              </ul>
            </div>
          @endif
        </ul>
      </div>
    </div>
  </div>
  @yield('content')
</div>