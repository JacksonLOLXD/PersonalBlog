<ul class="separator">

  <li>
    <a class="" href="{{ url('/user') }}">
      <i class="icons fa fa-users " aria-hidden="true"></i>
      <span class="item-menu">Perfil</span>
    </a>
  </li>
  <li>
    <a class="" href="{{ url('/post') }}">
      <i class="icons fa fa-bar-chart" aria-hidden="true"></i>
      <span class="item-menu">Publicaciones</span>
    </a>
  </li>

<!--
  <li>
    <a class="" href="{{ url('/areaHeadquarter') }}">
      <i class="icons fa fa-sitemap " aria-hidden="true"></i>
      <span class="item-menu">Sedes</span>
    </a>
  </li>
  <li>
    <a class="" href="{{ url('/area') }}">
      <i class="icons fa fa-cubes" aria-hidden="true"></i>
      <span class="item-menu">Area</span>
    </a>
  </li>

  <li>
    <a class="" href="{{ url('/taskPeople') }}">
      <i class="icons fa fa-tasks" aria-hidden="true"></i>
      <span class="item-menu">Funciones</span>
    </a>
  </li>
-->
</ul>
